import importscsv
import pdfexcel
import argparse
import os
import csv
import itertools
import datetime
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('--filename', required=True)
parser.add_argument('--year', required=True)
parser.add_argument('--month', required=True)
parser.add_argument('--username', required=True)
parser.add_argument('--userpnr', required=True)
parser.add_argument('--organizername', required=True)
parser.add_argument('--organizerpnr', required=True)
parser.add_argument('--companyname', required=True)
parser.add_argument('--telnr', required=True)
args = parser.parse_args()

print(f'filename: {args.filename}')
print(f'year: {args.year}')
print(f'month: {args.month}')
for arg in args:
    print(f'{arg}' {vars(args)[arg]})

for file in [a for a in os.listdir('csvfiles')]:
    os.remove(Path(os.getcwd()+'/csvfiles/'+file))

print(f'importing from {args.filename}')
importscsv.write_to_csv(args.filename)
print(f'{args.filename} imported')

for file in [a for a in os.listdir('csvfiles') if a.split(".")[-1] == 'csv']:
    print(f'currently handling: {file}')
    os.rename(Path(os.getcwd()+'/csvfiles/'+file),Path(os.getcwd()+'/csvfiles/'+file.split(".")[0].split("; ")[0]+'.csv'))
    pdfexcel.create_pdf(file.split(".")[0].split("; ")[0])
    pdfexcel.fill_pdf(year=args.year,
                      month=args.month,
                      assistent=file.split(".")[0].split("; ")[0],
                      asspnr=file.split(".")[0].split("; ")[1],
                      asstel=file.split(".")[0].split("; ")[2],
                      user_name=args.username,
                      user_pnr=args.userpnr,
                      organizer_name=args.organizername,
                      organizer_pnr=args.organizerpnr,
                      company_name=args.companyname,
                      telnr=args.telnr)

for file in os.listdir(Path(os.getcwd()+'/test')):
    os.remove(Path(os.getcwd()+'/test/'+file))
    os.remove(Path(os.getcwd()+'/'+file))
    #print(f'{file} removed from prep-folders')



# Create report for hours worked

from pdfexcel import convert_to_time

# Creating the base list of dates for report
schedule = [[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],
            [20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31]]

for i in range(len(schedule)):
    for j in range(len(schedule[i])):
        schedule[i][j] = str(schedule[i][j])

# Reading working times from csv files
for file in [a for a in os.listdir('csvfiles') if a.split(".")[-1] == 'csv']:
    print(f"adding hours from {file} to report")
    with open(Path(os.getcwd()+'/csvfiles/'+file),'r') as f:
        reader = csv.reader(f)
        for row in reader:
            for i in range(len(schedule)): # This can be made a lot more effective
                if row == []:
                    continue
                if row[0] == schedule[i][0]:
                    try:
                        if row[1] != '':
                            schedule[i].append([row[1],row[2],file.split(".")[0]])
                    except IndexError:
                        pass

# Writing working times to csv file
with open(os.getcwd()+'/report.csv','w',newline='',encoding="ISO-8859-1") as f:
    writer = csv.writer(f)
    writer.writerow(['Datum','Status','Pass 1 tid','Pass 1 ass','Pass 2 tid','Pass 2 ass','Pass 3 tid','Pass 3 ass','Pass 4 tid','Pass 4 ass','Pass 5 tid','Pass 5 ass','Pass 6 tid','Pass 6 ass','Pass 7 tid','Pass 7 ass','Pass 8 tid','Pass 8 ass'])
    for row in schedule:
        n = [row[0],""]

        for i in range(len(row))[1:]:
            row[i][0] = convert_to_time(row[i][0])
            row[i][1] = convert_to_time(row[i][1])

        from operator import itemgetter
        #print(sorted(row[1:], key=itemgetter(0) ))

        row = sorted(row[1:], key=itemgetter(0) )

        for i in range(len(row)):
            n += [row[i][0] + "-" + row[i][1], row[i][2]]
                    
        writer.writerow(n)

# Function which takes a list of pairs of datetime.times (start_time,end_time) and 
#   1. checks them pairwise for overlap
#   2. checks them for complete overlap
def check_overlap(l: list) -> bool:
    for pair in list(itertools.combinations(l, 2)):
        #print(f"Testing {pair}")
        if pair[0][0] < pair[1][0] < pair[0][1] or pair[0][0] < pair[1][1] < pair[0][1]:
            print("ERROR: Overlap")
            input("")
            return True
        if pair[0] == pair[1]:
            print("ERROR: Complete overlap")
            input("")
            return True
    return False

# Function which takes a list of pairs of datetime.times and checks if their timedelta is a full day
def check_duration(l: list) -> bool:
    time = 0
    for pair in l:
        # Create datetime objects for each time (a and b)
        dateTimeA = datetime.datetime.combine(datetime.date.today(), pair[0])
        dateTimeB = datetime.datetime.combine(datetime.date.today(), pair[1])
        # Get the difference between datetimes (as timedelta)
        dateTimeDifference = dateTimeB - dateTimeA
        # Divide difference in seconds by number of seconds in hour (3600)  
        dateTimeDifferenceInHours = dateTimeDifference.total_seconds() / 3600
        time += dateTimeDifferenceInHours
    if round(time) != 24:
        print(f"ERROR: Summation of times produced error. Summed to {round(time)}")
        input("")
        return True
    return False


# Checking for correct times
with open(os.getcwd()+'/report.csv','r',encoding="ISO-8859-1") as f:
    reader = csv.reader(f)
    for row in reader:
        if row[0] != 'Datum':
            print(f"Testing day {row[0]}")
            pairs = [(datetime.time.fromisoformat(row[i].split("-")[0].replace(".",":")), 
                               datetime.time.fromisoformat(row[i].split("-")[1].replace(".",":").replace('24:00','23:59'))
                               ) for i in range(2, len(row), 2)]
            if check_overlap(pairs) or check_duration(pairs):
                pass
            else:
                print(f"No errors found day {row[0]}")


# Filling second form

pdfexcel.fill_pdf2(year=args.year,
                   month=args.month,
                   filename=args.filename,
                   user_name=args.username,
                   user_pnr=args.userpnr,
                   organizer_name=args.organizername,
                   organizer_pnr=args.organizerpnr,
                   telnr=args.telnr)
