import csv
from pathlib import Path

def create(filename):
    l = []
    with open(filename, encoding="ISO-8859-1") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            l.append(row)

    def subcreate(start):
        tmp = []
        tmp.append([l[0][start],'',''])
        for i in range(2,33):
            tmp.append([l[i][0],l[i][start],l[i][start+1]])
            tmp.append([l[i][0],l[i][start+2],l[i][start+3]])
        return tmp
    import itertools
    #return [subcreate(i) for i in itertools.chain(range(10,142,11),range(142,len(row),12))]
    #print([i for i in itertools.chain([a for a in range(10,len(row),11) if l[0][a] != ''],[a for a in range(10,len(row),12) if l[0][a] != ''])])
    #return [subcreate(i) for i in itertools.chain([a for a in range(10,len(row),11) if l[0][a] != ''],[a for a in range(10,len(row),12) if l[0][a] != ''])]
    return [subcreate(i) for i in range(10,len(row)) if len(l[0][i]) > 10]


def write_to_csv(filename):
    import os
    try:
        os.mkdir('csvfiles')
    except FileExistsError:
        pass
    for i in create(filename=filename):
        with open(Path('csvfiles/'+str(i[0][0])+'.csv'),'w', encoding="ISO-8859-1") as csvfile:
            writer = csv.writer(csvfile)
            for r in i:
                if r == i[0]:
                    continue
                writer.writerow(r)
