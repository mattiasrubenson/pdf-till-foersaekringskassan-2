# PDF till Försäkringskassan

Skapar ifyllda PDF-blanketter till Försäkringskassan från en CSV-fil med assistenternas namn, personnummer, telefonnummer och arbetstider.

## Setup
setup.ps1 är ett PowerShell-script som installerar Python 3.10 och PDFtk, samt lägger dem i PATH.

Programmet använder en fork av python-paketet pdfforms. Pdfforms-fk finns här: https://gitlab.com/mattiasrubenson/pdfforms-fk. Se till att ersätta pdfforms med pdfforms-fk i dina installerade paket.

## Guide till användning
För att använda programmet navigerar du till installationsmappen och kör `python3 app.py --year YEAR --month MONTH --filename FILENAME`, där `YEAR` är aktuellt årtal, `MONTH` är aktuell månad (så som du vill att den skrivs i blanketten) och `FILENAME` är namnet på csv-filen med input (inklusive filändelse).

Ifyllda blanketter hamnar till slut i mappen filled.

## Format för CSV-input

## Hur funkar det?

## TODO

## License
GPLv3
