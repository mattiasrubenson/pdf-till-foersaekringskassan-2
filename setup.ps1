Start-Process ./python-installer.exe -Wait
Start-Process ./pdftk-installer.exe -Wait
pip install --no-cache-dir -r requirements.txt
$path = where.exe python
try {$path = $path[0]} catch {"Error getting Python PATH";exit}
$path = $path.replace('python.exe','')
$path += "Lib\site-packages\pdfforms"
cd editpdfforms
Get-ChildItem | ForEach-Object {Copy-Item $_.Name $path}
$p = Read-Host "Where did you install pdftk? If in 'C:\Program Files (x86)\PDFtk\bin\', type N, otherwise the path to PDFtk\bin"
if ($p == 'N') {$env:Path += ";C:\Program Files (x86)\PDFtk\bin\"} else {$env:Path += ";"+$p}
