from fillpdf import fillpdfs
from pathlib import Path

import pikepdf
import os
import datetime

def convert_to_time(x):
    x = x.split(",")
    if len(x[0]) == 1:
        x[0] = '0'+x[0]
    if x[1] == '00' or x[1] == '0':
        return x[0]+".00"
    else:
        print(x)
        tmp = x[0] + "." + str(int(60 / (100 / int(x[1]))))
        if len(tmp.split(".")[-1]) == 1:
            tmp += "0"
        return tmp

def create_pdf(assistent):

    pdf = pikepdf.open('fk.pdf')
    pdf.save(assistent+'.pdf')

def fill_pdf(year,month,assistent,asspnr,asstel,user_name,user_pnr,organizer_name,organizer_pnr,telnr,company_name):
    import pdfforms
    import csv
    
    for a in pdfforms.inspect_pdfs([str(Path(os.getcwd()+'/'+assistent+'.pdf'))]):pass
    l = []
    with open(Path('csvfiles/'+assistent+'.csv'), encoding="ISO-8859-1") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            if row == []:
                continue
            if row[1] == '' and row[2] == '':
                pass
            if all(i == ' ' for i in row[1]) and all(i == ' ' for i in row[2]):
                pass
            else:
                row[1] = convert_to_time(row[1])
                row[2] = convert_to_time(row[2])
                l.append(row)

    with open('data.csv','w') as f:
        writer = csv.writer(f)
        writer.writerow([Path(os.getcwd()+'/'+assistent+'.pdf'),'',''])
        writer.writerow([11,'',2])
        start = 36

        sum_of_time = 0

        for r in l:
            writer.writerow([start,'',r[0]])
            writer.writerow([start+1,'',r[1]])
            writer.writerow([start+2,'',r[2]])
            writer.writerow([start+3,'',1])
            if start == 150:
                start += 7
            else:
                start += 6
            from datetime import datetime
            FMT = '%H.%M'
            if r[2] == '24.00':
                sum_of_time += (datetime.strptime('23.59', FMT) - datetime.strptime(r[1], FMT)).seconds + 60
            else:
                sum_of_time += (datetime.strptime(r[2],FMT) - datetime.strptime(r[1],FMT)).seconds

        writer.writerow([0, '', str(year[0])])
        writer.writerow([1, '', str(year[1])])
        writer.writerow([2, '', str(year[2])])
        writer.writerow([3, '', str(year[3])])

        writer.writerow([26,'',user_name])
        writer.writerow([10,'',user_pnr])
        writer.writerow([8,'',assistent])
        writer.writerow([9,'',asspnr])

        writer.writerow([27,'',2])
        writer.writerow([33,'',company_name])
        writer.writerow([32,'',organizer_name])
        writer.writerow([34,'',telnr])
        writer.writerow([278,'',user_pnr])
        writer.writerow([279,'',asspnr])
        writer.writerow([25,'',telnr])
        writer.writerow([29,'',organizer_pnr])
        writer.writerow([532,'',asspnr])
        writer.writerow([531,'',user_pnr])

        writer.writerow([525,'',str(sum_of_time/60/60).split(".")[0]])

        writer.writerow([39,'',1])

        writer.writerow([282,'',str(asstel)])

            # If env var %assdat% and %arrdat% are set, the date is written from them, 
            # otherwise current date is written
        import datetime
        today = datetime.date.today().isoformat()
        writer.writerow([281,'',os.getenv('assdat',today)])
        writer.writerow([24,'',os.getenv('arrdat',today)])
        
        try:
            tmp = str(int(60/(100/int(str(sum_of_time/60/60).split(".")[1]))))
            if len(tmp) == 1:
                tmp += '0'
            writer.writerow([526,'',tmp])
        except:
            writer.writerow([526,'','0'])

        match month:
            case 'Januari':
                writer.writerow([12,'',year+'-01-01'])
                writer.writerow([13,'',year+'-02-28'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '1'])
                writer.writerow([280,'',year+'-'+'01'])
                writer.writerow([533,'',year+'-'+'01'])
            case 'Februari':
                writer.writerow([12,'',year+'-01-01'])
                writer.writerow([13,'',year+'-02-28'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '2'])
                writer.writerow([280,'',year+'-'+'02'])
                writer.writerow([533,'',year+'-'+'02'])
            case 'Mars':
                writer.writerow([12, '', year + '-03-01'])
                writer.writerow([13, '', year + '-04-30'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '3'])
                writer.writerow([280,'',year+'-'+'03'])
                writer.writerow([533,'',year+'-'+'03'])
            case 'April':
                writer.writerow([12, '', year + '-03-01'])
                writer.writerow([13, '', year + '-04-30'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '4'])
                writer.writerow([280,'',year+'-'+'04'])
                writer.writerow([533,'',year+'-'+'04'])
            case 'Maj':
                writer.writerow([12, '', year + '-05-01'])
                writer.writerow([13, '', year + '-06-30'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '5'])
                writer.writerow([280,'',year+'-'+'05'])
                writer.writerow([533,'',year+'-'+'05'])
            case 'Juni':
                writer.writerow([12, '', year + '-05-01'])
                writer.writerow([13, '', year + '-06-30'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '6'])
                writer.writerow([280,'',year+'-'+'06'])
                writer.writerow([533,'',year+'-'+'06'])
            case 'Juli':
                writer.writerow([12, '', year + '-07-01'])
                writer.writerow([13, '', year + '-08-31'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '7'])
                writer.writerow([280,'',year+'-'+'07'])
                writer.writerow([533,'',year+'-'+'07'])
            case 'Augusti':
                writer.writerow([12, '', year + '-07-01'])
                writer.writerow([13, '', year + '-08-31'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '8'])
                writer.writerow([280,'',year+'-'+'08'])
                writer.writerow([533,'',year+'-'+'08'])
            case 'September':
                writer.writerow([12, '', year + '-09-01'])
                writer.writerow([13, '', year + '-10-31'])
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '9'])
                writer.writerow([280,'',year+'-'+'09'])
                writer.writerow([533,'',year+'-'+'09'])
            case 'Oktober':
                writer.writerow([12, '', year + '-09-01'])
                writer.writerow([13, '', year + '-10-31'])
                writer.writerow([4, '', '1'])
                writer.writerow([5, '', '0'])
                writer.writerow([280,'',year+'-'+'10'])
                writer.writerow([533,'',year+'-'+'10'])
            case 'November':
                writer.writerow([12, '', year + '-11-01'])
                writer.writerow([13, '', year + '-12-31'])
                writer.writerow([4, '', '1'])
                writer.writerow([5, '', '1'])
                writer.writerow([280,'',year+'-'+'11'])
                writer.writerow([533,'',year+'-'+'11'])
            case 'December':
                writer.writerow([12, '', year + '-11-01'])
                writer.writerow([13, '', year + '-12-31'])
                writer.writerow([4, '', '1'])
                writer.writerow([5, '', '2'])
                writer.writerow([280,'',year+'-'+'12'])
                writer.writerow([533,'',year+'-'+'12'])

    for a in pdfforms.fill_pdfs(data_file='data.csv',no_flatten=True,value_transforms=[]):pass


def fill_pdf2(year,month,filename,user_name,user_pnr,organizer_name,organizer_pnr,telnr):
    import pdfforms
    import csv

    for a in pdfforms.inspect_pdfs([str(Path(os.getcwd()+'/fk2.pdf'))]):pass

    with open('data.csv','w') as f:
    # Filling in the second form (3057)
        writer = csv.writer(f)
        writer.writerow([Path(os.getcwd()+'/fk2.pdf'),'',''])
        writer.writerow([0, '', str(year[0])])
        writer.writerow([1, '', str(year[1])])
        writer.writerow([2, '', str(year[2])])
        writer.writerow([3, '', str(year[3])])

        #from datetime import datetime
        #today = datetime.today().strftime("%Y-%m-%d")
        #writer.writerow([38, '', today])
        today = datetime.date.today().isoformat()
        writer.writerow([38,'',os.getenv('arrdat',today)])

        match month:
            case 'Januari':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '1'])
            case 'Februari':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '2'])
            case 'Mars':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '3'])
            case 'April':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '4'])
            case 'Maj':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '5'])
            case 'Juni':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '6'])
            case 'Juli':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '7'])
            case 'Augusti':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '8'])
            case 'September':
                writer.writerow([4, '', '0'])
                writer.writerow([5, '', '9'])
            case 'Oktober':
                writer.writerow([4, '', '1'])
                writer.writerow([5, '', '0'])
            case 'November':
                writer.writerow([4, '', '1'])
                writer.writerow([5, '', '1'])
            case 'December':
                writer.writerow([4, '', '1'])
                writer.writerow([5, '', '2'])
        

        writer.writerow([7,'',user_name])
        writer.writerow([6,'',user_pnr])
        with open(filename, encoding="ISO-8859-1") as p:
            reader = csv.reader(p)
            for row in reader:
                if row[0] == 'Summa':
                    print(f"Sum of hours: {row[2]}")
                    writer.writerow([15,'',row[2]])
                    break
        writer.writerow([16,'', '0'])
        writer.writerow([27,'', '0'])
        writer.writerow([28,'', '2'])
        writer.writerow([31,'', '1'])
        writer.writerow([33,'',organizer_name])
        writer.writerow([34,'',organizer_pnr])
        writer.writerow([36,'', '1'])
        writer.writerow([39,'', telnr])
        writer.writerow([44,'',user_pnr])


    for a in pdfforms.fill_pdfs(data_file='data.csv',no_flatten=True,value_transforms=[]):pass
